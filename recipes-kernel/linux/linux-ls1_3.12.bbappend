require linux-ls1-common.inc

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://ls1021aiot.dts \
            file://0001-fsnotify-fix-oops-in-fsnotify_clear_marks_by_group_f.patch \
            file://0002-watchdog-fix-errata-err004346.patch \
           "

# fix err: "linux-ls1-3.12-r0 do_deploy: Taskhash mismatch"
ZIMAGE_BASE_NAME[vardepsexclude] = "DATETIME"

do_configure_prepend() {
    cp -rf ${WORKDIR}/ls1021aiot.dts ${S}/arch/arm/boot/dts/ls1021aiot.dts
}

do_configure_append() {
    #
    # Add Enea specific kernel configs
    #

    #gather full config fragment paths
    addon_features=""
    if [ -n "${ENEA_KERNEL_FRAGMENTS}" ]; then
        for feature in ${ENEA_KERNEL_FRAGMENTS}; do
            addon_features="${addon_features} ${WORKDIR}/${feature}"
        done
    fi

    #in case there is no .config file yet
    touch ${B}/.config

    if [ -e "${S}/scripts/kconfig/merge_config.sh" ]; then
        O=${B} ${S}/scripts/kconfig/merge_config.sh -m .config ${addon_features}
    else
        bbfatal "No merge_config.sh found in the kernel tree."
    fi

    oe_runmake KCONFIG_ALLCONFIG=${B}/.config O=${B} alldefconfig
}
