require recipes-kernel/linux/enea-common.inc

ENEA_KERNEL_FRAGMENTS += "\
    cfg/localversion.cfg \
    cfg/with_modules.cfg \
    cfg/embedded.cfg \
    cfg/preempt.cfg \
    cfg/root_nfs.cfg \
    cfg/devtmpfs.cfg \
    cfg/bootlogd.cfg \
    cfg/mtd_tests.cfg \
    cfg/latencytop.cfg \
    cfg/ltp.cfg \
    cfg/fuse.cfg \
    cfg/dpa.cfg \
    cfg/kprobes.cfg \
    cfg/i2c.cfg \
    cfg/lttng.cfg \
    cfg/powertop.cfg \
    cfg/systemtap.cfg \
    cfg/kgdb.cfg \
    cfg/gpio.cfg \
    cfg/cpusets.cfg \
    cfg/nfsdv3.cfg \
    cfg/nfsdv4.cfg \
    cfg/nls_cp437.cfg \
    cfg/ikconfig.cfg \
    "
